THE FINAL RESCUE
A game by Joshua McLean

Originally developed for Ludum Dare 44

<http://mrjoshuamclean.com>
<http://8bitstoinfinity.com>

_________
\ Story \
 ---------
The world has ended and you're one of the many responsible for saving the last
survivors.

Money no longer matters. Food and people are the currency of the day.

To survive, you must have food. And people are the only thing you can trade for it.

But on the other hand, living passengers are a strain on the food supply.

___________
\ Controls \
 ------------

left stick - move
right stick - aim (when targeting)
left stick press - next camera angle
left trigger - brake
left bumper - toggle targeting
right bumper - capture
right trigger - ??
ABXY - shop
start - pause

___________
\ Roadmap \
 ------------

[Critical]
- Update controls with camera angle keys
- Add further away camera angle option
- Sound: Teleport ray miss

[Major]
- Better story presentation
- Multiple saved initial states for quickload (press number at title to load)
- F2/Right Stick Press: show passengers
- F3: Invert flight controls (display on title + messages)
- Sound: Sacrifice passenger

[Other]
- Autosave after shopping
- Alternative to function keys?
- Tutorial
- Esc/Start: pause menu (press again to return to title - currently goes
  straight to title)
- Sound: Flight ambience
- Select passengers to sacrifice
- Display (X, Z) coordinate position
- Multiple save slots (named saves?)
